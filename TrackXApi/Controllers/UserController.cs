﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TrackXApi.Enum;
using TrackXApi.Models.Request;
using TrackXApi.Models.Response;
using TrackXApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TrackXApi.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }



        [HttpGet("Users/{id}")]
        public IActionResult Get(int id)
        {
            var user = _userService.Get(id);
            return new JsonResult(user);
        }

        //books Driver with driverId
        [HttpPost("Users/{id}/rides")]
        public IActionResult RequestRide([FromBody] RideRequest ride)
        {
            _userService.CreateRide(ride);
            return Ok();

        }

        [HttpPut("Users/{id}/rides/{rideId?}")]
        public IActionResult UpdateRide(int id,int rideId,[FromQuery] int updatedRideStatus)
        {
            _userService.UpdateRide(id,rideId,updatedRideStatus);
            return Ok(rideId);
        }

        [HttpGet("Users/{id}/rides/{rideId?}")]
        public IActionResult GetRideDetails(int id, int rideId)
        {
            var rideDetails = _userService.GetRideDetails(id, rideId);
            return new JsonResult(rideDetails);

        }

        [HttpGet("Users/{id}/rides")]
        public IActionResult GetFiltredRides(int id, [FromQuery] int source, DateTime date)
        {
            var rides = (List<RideResponse>)_userService.GetAllRides(id);
            if (source != 0 && date != new DateTime())
            {
                var filteredBySource = rides.Where(ride => ride.Source == source && ride.RideRequestTime.Date == date.Date);
                return new JsonResult(filteredBySource);
            }
            else if (source != 0)
            {
                var filteredBySource = rides.Where(ride => ride.Source == source);
                return new JsonResult(filteredBySource);
            }
            else if (date != new DateTime())
            {
                var filteredBySource = rides.Where(ride => ride.RideRequestTime.Date == date.Date);
                return new JsonResult(filteredBySource);
            }
            return new JsonResult(rides);
        }


    }
}
