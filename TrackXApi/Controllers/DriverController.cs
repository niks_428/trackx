﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TrackXApi.Enum;
using TrackXApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TrackXApi.Controllers
{
    
    [ApiController]
    public class DriverController : ControllerBase
    {
        private readonly IDriverService _driverService;

        public DriverController(IDriverService driverService)
        {
            _driverService = driverService;
        }
        [Route("drivers")]
        [HttpGet("")]
        public IActionResult GetNearestFrom([FromQuery] int sourceLocation)
        {
            var drivers = _driverService.GetNearestFrom(sourceLocation);
            return new JsonResult(drivers);
        }


        [HttpGet("Drivers/{id}")]
        public IActionResult Get(int id)
        {
            var driver = _driverService.Get(id);
            return new JsonResult(driver);
        }

        //getRideDetails
        [HttpGet("Drivers/{id}/rides/{rideId}")]
        public IActionResult GetRideDetails(int id,int rideId)
        {
            var rideDetails = _driverService.GetRideDetails(id,rideId);
            return new JsonResult(rideDetails);

        }

        [HttpPut("Drivers/{id}/rides/{rideId}")]
        public IActionResult DriverActionOnRide(int id,int rideId, [FromQuery] int status = 0)
        {
            if(status==(int)RideStatus.Accepted || status == (int)RideStatus.Rejected)
            {
                _driverService.DriverActionOnNewRide(id, rideId, status);

            }else if(status == (int)RideStatus.Riding)
            {
                _driverService.DriverActionOnAcceptedRide(id, rideId, status);
            }
            else if (status == (int)RideStatus.Completed || status==(int)RideStatus.Uncompleted)
            {
                _driverService.DriverActionOnOngoingRide(id, rideId, status);
            }
            else
            {
                return BadRequest();
            }
            return Ok(rideId);
        }

    }
}
