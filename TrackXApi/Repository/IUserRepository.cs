﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackXApi.Models.DB;
using TrackXApi.Models.Response;

namespace TrackXApi.Repository
{
    public interface IUserRepository
    {
        public UserResponse Get(int id);


        public void Post(
           string Name,
           string EmailId,
           long PhoneNo,
           DateTime DOB,
           string Gender);


    }
}
