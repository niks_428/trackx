﻿using System;
using Microsoft.Extensions.Options;
using TrackXApi.Models.DB;
using System.Data.SqlClient;
using Dapper;
using TrackXApi.Models.Response;

namespace TrackXApi.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly ConnectionString _connectionString;

        public UserRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }

        public UserResponse Get(int id)
        {
            const string query = @"
SELECT Id
	,Name
    ,CurrentStatus
FROM Users 
WHERE Id= @id ";

            using var connection = new SqlConnection(_connectionString.DB);
            var user = connection.QueryFirst<UserResponse>(query, new { Id = id });
            return user;
        }

        public void Post(string Name, string EmailId, long PhoneNo, DateTime DOB, string Gender)
        {
            throw new NotImplementedException();
        }
    }
}
