﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackXApi.Enum;
using TrackXApi.Models.DB;
using TrackXApi.Models.Request;
using TrackXApi.Models.Response;

namespace TrackXApi.Repository
{
    public interface IRideRepository
    {
        public Ride Get(int id);

        public void Post(RideRequest ride);
        public void UpdateRideStatus(int userId,int driverId, int rideId, CancelledBy cancelledBy, RideStatus driveStatus);

        public IEnumerable<Ride> GetPendingRides(int driverId);

        public IEnumerable<Ride> GetByDate(int userId, DateTime date);
        public IEnumerable<Ride> GetAllByUserId(int id);
    }
}
