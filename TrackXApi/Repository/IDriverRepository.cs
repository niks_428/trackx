﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackXApi.Models.DB;
using TrackXApi.Models.Response;

namespace TrackXApi.Repository
{
    public interface IDriverRepository
    {
        public DriverResponse Get(int id);
        public IEnumerable<Driver> GetAll();
    }
}
