﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using TrackXApi.Enum;
using TrackXApi.Models.DB;
using TrackXApi.Models.Request;
using TrackXApi.Models.Response;

namespace TrackXApi.Repository
{
	public class RideRepository : IRideRepository
	{
		private ConnectionString _connectionString;
		public RideRepository(IOptions<ConnectionString> connectionString)
		{
			_connectionString = connectionString.Value;
		}

		public Ride Get(int id)
		{
			const string query = @"
SELECT * 
FROM Rides
WHERE Id=@RideId
";
			using var connection = new SqlConnection(_connectionString.DB);
			var ride = connection.QueryFirst<Ride>(query, new { RideId = id });
			return ride;
		}

		public void Post(RideRequest ride)
		{

			const string query = @"
EXEC CreateRide @UserId
	,@DriverId
	,@RideRequestTime
	,@Source
	,@Destination
";
			using var connection = new SqlConnection(_connectionString.DB);
			connection.Query(query, ride);
		}


		public void UpdateRideStatus(int userId,int driverId, int rideId, CancelledBy cancelledBy,RideStatus rideStatus)
		{
			const string query = @"
EXEC UpdateRideStatus @RideId
	,@CancelledBy
	,@DriverId
	,@UserId
	,@RideStatus
";
			using var connection = new SqlConnection(_connectionString.DB);
			connection.Query(query, new { RideId=rideId,CancelledBy=(int)cancelledBy,DriverId=driverId,UserId=userId,RideStatus=(int)rideStatus});
		}


		public IEnumerable<Ride> GetAllByUserId(int id)
		{

			const string query = @"
SELECT * 
FROM Rides
WHERE UserId=@UserId
";
			using var connection = new SqlConnection(_connectionString.DB);
			var rides=connection.Query<Ride>(query, new { UserId=id });
			return rides;
		}
















		public IEnumerable<Ride> GetByDate(int userId,DateTime date)
		{

				const string query = @"
SELECT * 
FROM Rides
WHERE RideRequestTime=@Date
AND UserId=@UserId
";

				using var connection = new SqlConnection(_connectionString.DB);
				var rides = connection.Query<Ride>(query,new { Date=date,UserId=userId});
				return rides;
		}



		public IEnumerable<Ride> GetPendingRides(int driverId)
		{
			const string query = @"
SELECT * 
FROM Rides
WHEREDriverId=@DriverId
AND RideStatus=1
";

			using var connection = new SqlConnection(_connectionString.DB);
			var rides = connection.Query<Ride>(query, new { DriverId = driverId });
			return rides;
		}

    }
}
