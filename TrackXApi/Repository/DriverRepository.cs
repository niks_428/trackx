﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using TrackXApi.Models.DB;
using TrackXApi.Models.Response;

namespace TrackXApi.Repository
{
    public class DriverRepository : IDriverRepository
    {
        private readonly ConnectionString _connectionString;

        public DriverRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }

        public DriverResponse Get(int id)
        {
            const string query = @"
SELECT Id
	,Name
    ,CurrentStatus
    ,CurrentLocation
FROM Drivers 
WHERE Id= @id ";

            using var connection = new SqlConnection(_connectionString.DB);
            var driver = connection.QueryFirst<DriverResponse>(query, new { Id = id });
            return driver;
        }

        public IEnumerable<Driver> GetAll()
        {
            const string query = @"
SELECT Id
	,Name
	,EmailId
    ,Phone
	,DOB
	,Gender
    ,CurrentStatus
    ,CurrentLocation
FROM Drivers ";

            using var connection = new SqlConnection(_connectionString.DB);
            var drivers = connection.Query<Driver>(query);
            return drivers;
        }
    }
}
