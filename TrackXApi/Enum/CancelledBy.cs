﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackXApi.Enum
{
    public enum CancelledBy
    {
        User=1,
        Driver=2,
        System=3,
        None=4
    }
}
