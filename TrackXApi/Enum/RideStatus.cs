﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackXApi.Enum
{
    public enum RideStatus
    {
        Pending=1,   
        Rejected=2,  
        Accepted=3,
        Riding=4,    
        Completed=5, 
        Uncompleted=6
    }
}
