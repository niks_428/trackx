﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackXApi.Enum
{
    public enum PersonCurrentStatus
    {
        Idle=0,
        Riding=1,
        Requested=3
    }
}
