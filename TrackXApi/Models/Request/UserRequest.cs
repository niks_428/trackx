﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackXApi.Models.Request
{
    public class UserRequest
    {
        public string Name { get; set; }
        public string emailId { get; set; }
        public long Phone { get; set; }
        public DateTime Dob { get; set; }
        public string Gender { get; set; }
    }
}
