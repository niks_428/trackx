﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackXApi.Models.Request
{
    public class RideRequest
    {
        public int UserId { get; set; }
        public int DriverId { get; set; }
        public DateTime RideRequestTime { get; set; }
        public int Source { get; set; }
        public int Destination { get; set; }

    }
}
