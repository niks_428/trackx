﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackXApi.Models.Response
{
    public class RideResponse
    {
        public int Id { get; set; }
        public UserResponse User { get; set; }
        public DriverResponse Driver { get; set; }
        public DateTime RideRequestTime { get; set; }
        public int RideStatus { get; set; }
        public double TotalFare { get; set; }
        public int CancelledBy { get; set; }
        public int Source { get; set; }
        public int Destination { get; set; }
    }
}
