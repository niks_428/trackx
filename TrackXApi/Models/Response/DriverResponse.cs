﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackXApi.Models.Response
{
    public class DriverResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CurrentStatus { get; set; }

        public int CurrentLocation { get; set; }

    }
}
