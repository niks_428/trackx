﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackXApi.Models.DB
{
    public class Person
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
        public long Phone { get; set; }
        public DateTime Dob { get; set; }
        public string Gender { get; set; }
        public int CurrentStatus { get; set; }

    }
}
