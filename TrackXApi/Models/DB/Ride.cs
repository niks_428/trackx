﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackXApi.Models.DB
{
    public class Ride
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int DriverId { get; set; }
        public DateTime RideRequestTime { get; set; }
        public DateTime RideAcceptTime { get; set; }
        public DateTime RideStartTime { get; set; }
        public DateTime RideEndTime { get; set; }
        public DateTime RideCancelTime { get; set; }
        public int? CancelledBy { get; set; }
        public int RideStatus { get; set; }
        public double TotalFare { get; set; }
        public int Source { get; set; }
        public int Destination { get; set; }

    }
}
