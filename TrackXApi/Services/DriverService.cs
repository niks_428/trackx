﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrackXApi.Enum;
using TrackXApi.Models.DB;
using TrackXApi.Models.Response;
using TrackXApi.Repository;

namespace TrackXApi.Services
{
    public class DriverService : IDriverService
    {
        private readonly IDriverRepository _driverRepository;
        private readonly IRideRepository _rideRepository;
        private readonly IUserRepository _userRepository;

        public DriverService(IDriverRepository driverRepository,IRideRepository rideRepository,IUserRepository userRepository)
        {
            _driverRepository = driverRepository;
            _rideRepository = rideRepository;
            _userRepository = userRepository;
        }

        public DriverResponse Get(int id)
        {
            var driver = _driverRepository.Get(id);
            if (driver == null)
            {
                throw new EntityNotFoundException(string.Format("Driver with Id {0} Not Available", id));
            }
            return driver;
        }


            public IEnumerable<DriverResponse> GetNearestFrom(int location)
        {
            IEnumerable<Driver> drivers = _driverRepository.GetAll();
            if (!drivers.Any())
            {
                throw new EntityNotFoundException(string.Format("No Driver Available"));

            }

            IEnumerable<DriverResponse> nearestDrivers;

            var leftDis = location - 5;
            var rightDis = location + 5;

            nearestDrivers = drivers.Where(driver => ((driver.CurrentLocation >= leftDis && driver.CurrentLocation <= rightDis) && driver.CurrentStatus == (int)PersonCurrentStatus.Idle)).Select(driver => new DriverResponse
            {
                Id=driver.Id,
                Name=driver.Name

            });

            if (!nearestDrivers.Any())
            {
                throw new EntityNotFoundException(string.Format("No Driver Available Nearby"));
            }
            return nearestDrivers;
        }

        public RideResponse GetRideDetails(int id, int rideId)
        {
            var ride = _rideRepository.Get(rideId);
            if (ride==null)
            {
                throw new EntityNotFoundException(string.Format("No Ride with ID {0} Available",rideId));
            }
            var rideResponse = new RideResponse();
            rideResponse.Id = ride.Id;
            rideResponse.User = _userRepository.Get(ride.UserId);
            rideResponse.Driver = _driverRepository.Get(ride.UserId);
            rideResponse.Source = ride.Source;
            rideResponse.Destination = ride.Destination;
            rideResponse.TotalFare = ride.TotalFare;
            rideResponse.RideStatus = ride.RideStatus;

            return rideResponse;
        }

        public void DriverActionOnNewRide(int driverId,int rideId, int status)
        {
            var ride = _rideRepository.Get(rideId);
            if (ride.DriverId != driverId || ride.RideStatus != (int)RideStatus.Pending)
            {
                throw new EntityNotFoundException(string.Format("No Ride with ID {0} Availablle for You Currently", rideId));
            }
            var userId = ride.UserId;
            switch (status)
            {
                case (int) RideStatus.Accepted:
                    _rideRepository.UpdateRideStatus(userId,driverId,rideId,CancelledBy.None, RideStatus.Accepted);
                    break;
                case (int)RideStatus.Rejected:
                    _rideRepository.UpdateRideStatus(userId, driverId, rideId, CancelledBy.Driver, RideStatus.Rejected);
                    break;
                default:
                    throw new EntityNotFoundException("Only Accept And Reject Possible");
            }

        }

        public void DriverActionOnOngoingRide(int driverId, int rideId, int status)
        {
            var ride = _rideRepository.Get(rideId);
            if (ride.DriverId != driverId || ride.RideStatus != (int)RideStatus.Riding)
            {
                throw new EntityNotFoundException(string.Format("Can Only Perform Action on Your Ongoing Ride", rideId));
            }
            var userId = ride.UserId;
            switch (status)
            {
                case (int)RideStatus.Completed:
                    _rideRepository.UpdateRideStatus(userId, driverId, rideId, CancelledBy.None, RideStatus.Completed);
                    break;
                case (int)RideStatus.Uncompleted:
                    _rideRepository.UpdateRideStatus(userId, driverId, rideId, CancelledBy.Driver, RideStatus.Uncompleted);
                    break;
                default:
                    throw new EntityNotFoundException("Only Complete  And Uncomplete Possible");
            }
        }

        public void DriverActionOnAcceptedRide(int driverId, int rideId, int status)
        {
            var ride = _rideRepository.Get(rideId);
            if (ride.DriverId != driverId || ride.RideStatus != (int)RideStatus.Accepted)
            {
                throw new EntityNotFoundException("Can Only Perform Action on Your Accepted Ride Only");
            }
            var userId = ride.UserId;
            switch (status)
            {
                case (int)RideStatus.Riding:
                    _rideRepository.UpdateRideStatus(userId, driverId, rideId, CancelledBy.None, RideStatus.Riding);
                    break;
                default:
                    throw new EntityNotFoundException("Only Starting the Ride Possible");
            }
        }




        public IEnumerable<RideResponse> GetBookings(int driverId)
        {
            throw new NotImplementedException();
        }
    }
}
