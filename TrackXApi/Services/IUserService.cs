﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackXApi.Enum;
using TrackXApi.Models.Request;
using TrackXApi.Models.Response;

namespace TrackXApi.Services
{
    public interface IUserService
    {

        public UserResponse Get(int id);

        public void CreateRide(RideRequest ride);
        public void UpdateRide(int userId, int rideId,int rideStatus);
        public RideResponse GetRideDetails(int id, int rideId);

        public IEnumerable<RideResponse> GetAllRides(int id);
    }
}
