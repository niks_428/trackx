﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackXApi.Models.Response;

namespace TrackXApi.Services
{
    public interface IDriverService
    {
        public DriverResponse Get(int id);
        public IEnumerable<DriverResponse> GetNearestFrom(int location);
        public RideResponse GetRideDetails(int id, int rideId);


        public void DriverActionOnNewRide(int driverId,int rideId, int status);

        public IEnumerable<RideResponse> GetBookings(int driverId);
        void DriverActionOnOngoingRide(int id, int rideId, int status);
        void DriverActionOnAcceptedRide(int id, int rideId, int status);
    }
}
