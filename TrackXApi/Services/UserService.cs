﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackXApi.Enum;
using TrackXApi.Models.Request;
using TrackXApi.Models.Response;
using TrackXApi.Repository;

namespace TrackXApi.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IRideRepository _rideRepository;
        private readonly IDriverRepository _driverRepository;


        public UserService(IUserRepository userRepository,IRideRepository rideRepository,IDriverRepository driverRepository)
        {
            _userRepository = userRepository;
            _rideRepository = rideRepository;
            _driverRepository = driverRepository;
        }

        public UserResponse Get(int id)
        {
            var user = _userRepository.Get(id);
            if (user==null)
            {
                throw new EntityNotFoundException(string.Format("User with Id {0} Not Available", id));
            }
            
            return user;
        }

        public void CreateRide(RideRequest ride)
        {
            var user = _userRepository.Get(ride.UserId);
            var driver = _driverRepository.Get(ride.DriverId);
            if (user.CurrentStatus == (int)PersonCurrentStatus.Idle)
            {
                if (driver.CurrentStatus != (int)PersonCurrentStatus.Idle)
                {
                    throw new EntityNotFoundException(string.Format("Selected Driver Not Available for Booking"));
                }
                _rideRepository.Post(ride);
            }
            else
            {
                throw new EntityNotFoundException(string.Format("Please Wait for Previous Ride to complete Or Cancel It"));
            }
        }
        public void UpdateRide(int userId, int rideId, int newRideStatus)
        {
            var ride = _rideRepository.Get(rideId);
            if (ride.UserId != userId || ride.RideStatus==(int)RideStatus.Rejected )
            {
                throw new EntityNotFoundException(string.Format("No Ride with ID {0} Availablle for You", rideId));
            }
            var driverId = ride.DriverId;

            if(ride.RideStatus==(int)RideStatus.Completed || ride.RideStatus == (int)RideStatus.Riding)
            {
                throw new EntityNotFoundException("Ride Completed Or in Progress");
            }
            switch (newRideStatus)
            {
                case (int)RideStatus.Rejected:
                    _rideRepository.UpdateRideStatus(userId, driverId, rideId, CancelledBy.Driver, RideStatus.Rejected);
                    break;
                default:
                    throw new EntityNotFoundException("Only Cancel Possible Ror user");
            }
        }

        public RideResponse GetRideDetails(int id, int rideId)
        {
            var ride = _rideRepository.Get(rideId);
            if (ride == null)
            {
                throw new EntityNotFoundException(string.Format("No Ride with ID {0} Available", rideId));
            }else if (ride.UserId != id)
            {
                throw new EntityNotFoundException(string.Format("Ride Not Associated with You", rideId));
            }
            var rideResponse = new RideResponse();
            rideResponse.Id = ride.Id;
            rideResponse.User = _userRepository.Get(ride.UserId);
            rideResponse.Driver = _driverRepository.Get(ride.UserId);
            rideResponse.Source = ride.Source;
            rideResponse.Destination = ride.Destination;
            rideResponse.TotalFare = ride.TotalFare;
            rideResponse.RideStatus = ride.RideStatus;

            return rideResponse;
        }

        public IEnumerable<RideResponse> GetAllRides(int id)
        {
            var rides = _rideRepository.GetAllByUserId(id);
            if (rides == null)
            {
                throw new EntityNotFoundException("No Rides Booked Yet");
            }
            var rideResponses = new List<RideResponse>();

            foreach(var ride in rides)
            {
                var rideResponse = new RideResponse();
                rideResponse.Id = ride.Id;
                rideResponse.User = _userRepository.Get(ride.UserId);
                rideResponse.Driver = _driverRepository.Get(ride.UserId);
                rideResponse.Source = ride.Source;
                rideResponse.Destination = ride.Destination;
                rideResponse.TotalFare = ride.TotalFare;
                rideResponse.RideStatus = ride.RideStatus;
                rideResponse.Source = ride.Source;
                rideResponse.Destination = ride.Destination;
                rideResponse.RideRequestTime = ride.RideRequestTime;
                rideResponses.Add(rideResponse);
            }

            return rideResponses;
        }

    }
}
