﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using TechTalk.SpecFlow;
using TrackXApi;
using Xunit;

namespace TrackXTest.Steps
{
    [Binding]
    class BaseSteps
    {
        protected HttpClient Client { get; set; }
        protected WebApplicationFactory<TestStartup> Factory;
        protected HttpResponseMessage httpResponseMessage;
        public HttpContent httpContent;
        protected StringContent stringContent;

        public BaseSteps(WebApplicationFactory<TestStartup> baseFactory)
        {
            Factory = baseFactory;
        }

        [Given(@"I am a client")]
        public void GivenIAmAClient()
        {
            Client = Factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                BaseAddress = new Uri("http://localhost")
            });
        }

        [When(@"I Make GET Request '(.*)'")]
        public async Task WhenIMakeGETRequestAsync(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            httpResponseMessage = await Client.GetAsync(uri);
        }

        [Then(@"response code must be '(.*)'")]
        public void ThenResponseCodeMustBe(int statusCode)
        {
            var expectedStatusCode = (HttpStatusCode)statusCode;
            Assert.Equal(expectedStatusCode, httpResponseMessage.StatusCode);
        }

        [Then(@"response data must look like '(.*)'")]
        public async Task ThenResponseDataMustLookLikeAsync(string expectedResponse)
        {
            Console.WriteLine(httpResponseMessage.ToString());
            var responseData = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
            Assert.Equal(expectedResponse, responseData.ToString());
        }

        [When(@"data items are '(.*)'")]
        public void WhenDataItemsAre(string jsonData)
        {
            stringContent = new StringContent(jsonData, Encoding.UTF8, "application/json");
        }

        [When(@"I Make Post Request '(.*)'")]
        public async Task WhenIMakePostRequestAsync(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            httpResponseMessage = await Client.PostAsync(uri, stringContent).ConfigureAwait(false);
        }

        [When(@"I Make Put Request '(.*)'")]
        public async Task WhenIMakePutRequestAsync(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            httpResponseMessage = await Client.PutAsync(uri, stringContent);
        }

    }
}
