﻿using TechTalk.SpecFlow;
using TrackXApi;
using Microsoft.Extensions.DependencyInjection;
using TrackXTest.MockRepo;

namespace TrackXTest.Steps
{
    [Scope(Feature = "Driver Test")]
    [Binding]
    class DriverSteps : BaseSteps
    {
        public DriverSteps(CustomWebApplicationFactory<TestStartup> factory)
           : base(factory.WithWebHostBuilder(builder => {
               builder.ConfigureServices(services =>
               {
                   services.AddScoped(service => DriverMock.driverRepoMock.Object);
                   services.AddScoped(service => RideMock.rideRepoMock.Object);

               });
           }))
        {

        }

        [BeforeScenario]
        public void MockRepositories()
        {
            DriverMock.MockGetAll();
            DriverMock.MockGet();
            RideMock.MockGet();
        }
    }
}
