﻿using TechTalk.SpecFlow;
using TrackXApi;
using Microsoft.Extensions.DependencyInjection;
using TrackXTest.MockRepo;

namespace TrackXTest.Steps
{
    [Scope(Feature = "User Test")]
    [Binding]
    class UserSteps : BaseSteps
    {
        public UserSteps(CustomWebApplicationFactory<TestStartup> factory)
           : base(factory.WithWebHostBuilder(builder => {
               builder.ConfigureServices(services =>
               {
                   services.AddScoped(service => UserMock.userRepoMock.Object);
                   services.AddScoped(service => RideMock.rideRepoMock.Object);
               });
           }))
        {

        }

        [BeforeScenario("Get User By Id")]
        public void Get()
        {
            UserMock.MockGet();
            RideMock.MockGetByUserId();
        }
    }
}
