﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using TrackXApi.Models.DB;
using TrackXApi.Repository;

namespace TrackXTest.MockRepo
{
    class RideMock
    {
        public static readonly Mock<IRideRepository> rideRepoMock = new Mock<IRideRepository>();

        public static void MockGetByUserId()
        {
            List<Ride> entityList = new List<Ride>();

        entityList.Add(new Ride()
            {
                Id = 1,
                UserId = 1,
                DriverId=1,
                RideRequestTime= new DateTime(1999, 04, 04),
                RideAcceptTime = new DateTime(1999, 04, 04),
                RideStartTime = new DateTime(1999, 04, 04),
                RideEndTime = new DateTime(1999, 04, 04),
                RideCancelTime = new DateTime(1999, 04, 04),
                CancelledBy = 1,
                RideStatus=1,
                TotalFare=100.2,
                Source = 1,
                Destination = 5
        });
            entityList.Add(new Ride()
            {
                Id = 2,
                UserId = 2,
                DriverId = 2,
                RideRequestTime = new DateTime(1999, 04, 04),
                RideAcceptTime = new DateTime(1999, 04, 04),
                RideStartTime = new DateTime(1999, 04, 04),
                RideEndTime = new DateTime(1999, 04, 04),
                RideCancelTime = new DateTime(1999, 04, 04),
                CancelledBy = 1,
                RideStatus = 1,
                TotalFare = 100.2,
                Source=2,
                Destination=12
            });

            rideRepoMock.Setup(repo => repo.GetAllByUserId(It.IsAny<int>())).Returns((int id) =>
            {
                List<Ride> result = new List<Ride>();
                result.Add(entityList.Find(item => item.UserId == id));
                return result;
            });
        }

        public static void MockGet()
        {
            List<Ride> entityList = new List<Ride>();

            entityList.Add(new Ride()
            {
                Id = 1,
                UserId = 1,
                DriverId = 1,
                RideRequestTime = new DateTime(1999, 04, 04),
                RideAcceptTime = new DateTime(1999, 04, 04),
                RideStartTime = new DateTime(1999, 04, 04),
                RideEndTime = new DateTime(1999, 04, 04),
                RideCancelTime = new DateTime(1999, 04, 04),
                CancelledBy = 1,
                RideStatus = 1,
                TotalFare = 100.2,
                Source = 1,
                Destination = 5
            });
            entityList.Add(new Ride()
            {
                Id = 2,
                UserId = 2,
                DriverId = 2,
                RideRequestTime = new DateTime(1999, 04, 04),
                RideAcceptTime = new DateTime(1999, 04, 04),
                RideStartTime = new DateTime(1999, 04, 04),
                RideEndTime = new DateTime(1999, 04, 04),
                RideCancelTime = new DateTime(1999, 04, 04),
                CancelledBy = null,
                RideStatus = 1,
                TotalFare = 100.2,
                Source = 2,
                Destination = 12
            });

            entityList.Add(new Ride()
            {
                Id = 3,
                UserId = 1,
                DriverId = 1,
                RideRequestTime = new DateTime(1999, 04, 04),
                RideAcceptTime = new DateTime(1999, 04, 04),
                RideStartTime = new DateTime(1999, 04, 04),
                RideEndTime = new DateTime(1999, 04, 04),
                RideCancelTime = new DateTime(1999, 04, 04),
                CancelledBy = null,
                RideStatus = 4,
                TotalFare = 100.2,
                Source = 2,
                Destination = 12
            });

            rideRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                return entityList.Find(entity => entity.Id == id);

            });
        }
    }
}
