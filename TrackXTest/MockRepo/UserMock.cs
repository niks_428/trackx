﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using TrackXApi.Models.DB;
using TrackXApi.Models.Response;
using TrackXApi.Repository;

namespace TrackXTest.MockRepo
{
    public class UserMock
    {
        public static readonly Mock<IUserRepository> userRepoMock = new Mock<IUserRepository>();

        public static void MockGet()
        {
            List<UserResponse> entityList = new List<UserResponse>();

            entityList.Add(new UserResponse()
            {
                Id = 1,
                Name = "Nikhil1",
                CurrentStatus = 0
            });


            entityList.Add(new UserResponse()
            {
                Id = 2,
                Name = "Nikhil2",
                CurrentStatus = 0
            });

            entityList.Add(new UserResponse()
            {
                Id = 3,
                Name = "Nikhil3",
                CurrentStatus = 0
            });

            userRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                return entityList.Find(entity => entity.Id == id);
            });

        }
    }
}
