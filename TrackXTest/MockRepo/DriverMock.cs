﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using TrackXApi.Models.DB;
using TrackXApi.Models.Response;
using TrackXApi.Repository;

namespace TrackXTest.MockRepo
{
    class DriverMock
    {
        public static readonly Mock<IDriverRepository> driverRepoMock = new Mock<IDriverRepository>();

        public static void MockGetAll()
        {
            List<Driver> entityList = new List<Driver>();

            entityList.Add(new Driver()
            {
                Id = 1,
                Name = "Nikhil1",
                EmailId = "nik@hmsj.com",
                Phone = 17262728282,
                Gender = "Male",
                Dob = new DateTime(1999, 04, 04),
                CurrentStatus = 0,
                CurrentLocation=1
            });


            entityList.Add(new Driver()
            {
                Id = 2,
                Name = "Nikhil2",
                EmailId = "nik@hmsj.com",
                Phone = 17262728282,
                Gender = "Male",
                Dob = new DateTime(1999, 04, 04),
                CurrentStatus = 0,
                CurrentLocation=10
            });

            entityList.Add(new Driver()
            {
                Id = 3,
                Name = "Nikhil3",
                EmailId = "nik@hmsj.com",
                Phone = 17262728282,
                Gender = "Male",
                Dob = new DateTime(1999, 04, 04),
                CurrentStatus = 0,
                CurrentLocation=1
            });

            driverRepoMock.Setup(repo => repo.GetAll()).Returns(() =>
            {
                return entityList;
            });

        }

        public static void MockGet()
        {
            List<DriverResponse> entityList = new List<DriverResponse>();

            entityList.Add(new DriverResponse()
            {
                Id = 1,
                Name = "Driver1",
                CurrentStatus = 0,
                CurrentLocation=3
            });


            entityList.Add(new DriverResponse()
            {
                Id = 2,
                Name = "Driver2",
                CurrentStatus = 0
            });

            entityList.Add(new DriverResponse()
            {
                Id = 3,
                Name = "Driver3",
                CurrentStatus = 0
            });

            driverRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                return entityList.Find(entity => entity.Id == id);
            });

        }
    }
}
