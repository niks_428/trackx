﻿Feature: Driver Test


@getAllDriversInRange
Scenario: Get All Drivers Nearest
	Given I am a client
	When I Make GET Request '/drivers?sourceLocation=5'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Nikhil1","currentStatus":0,"currentLocation":0},{"id":2,"name":"Nikhil2","currentStatus":0,"currentLocation":0},{"id":3,"name":"Nikhil3","currentStatus":0,"currentLocation":0}]'

@getDriverById
Scenario: Get Driver By Id
	Given I am a client
	When I Make GET Request '/drivers/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Driver1","currentStatus":0,"currentLocation":3}'

@getRideAvailableForDriver
Scenario: Get Ride Avalable For Driver
	Given I am a client
	When I Make GET Request '/drivers/1/rides/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"user":{"id":1,"name":"Nikhil","currentStatus":3},"driver":{"id":1,"name":"Driver1","currentStatus":0,"currentLocation":3},"rideRequestTime":"0001-01-01T00:00:00","rideStatus":1,"totalFare":100.2,"cancelledBy":0,"source":1,"destination":5}'


@AcceptsRide
Scenario: Driver Accept Ride
	Given I am a client
	When I Make Put Request 'drivers/1/rides/1?status=3'
	Then response code must be '200'

@CompletesRide @AcceptsRide
Scenario: Driver Completes Ride
	Given I am a client
	When I Make Put Request 'drivers/1/rides/3?status=5'
	Then response code must be '200'



