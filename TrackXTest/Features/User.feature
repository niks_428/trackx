﻿Feature: User Test

@getUserById
Scenario: Get User By Id
	Given I am a client
	When I Make GET Request '/users/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Nikhil1","currentStatus":0}'

@getAllRidesforUser
Scenario: Get All rides for user
	Given I am a client
	When I Make GET Request '/users/1/rides'
	Then response code must be '200'
	And response data must look like '[{"id":1,"user":{"id":1,"name":"Nikhil1","currentStatus":0},"driver":{"id":1,"name":"N","currentStatus":0,"currentLocation":1},"rideRequestTime":"0001-01-01T00:00:00","rideStatus":1,"totalFare":100.2,"cancelledBy":0,"source":1,"destination":5}]'

@getAllRidesforUserBySourceLocation
Scenario: Get All rides for user by sourceLocation
	Given I am a client
	When I Make GET Request '/users/1/rides?source=1'
	Then response code must be '200'
	And response data must look like '[{"id":1,"user":{"id":1,"name":"Nikhil1","currentStatus":0},"driver":{"id":1,"name":"N","currentStatus":0,"currentLocation":1},"rideRequestTime":"0001-01-01T00:00:00","rideStatus":1,"totalFare":100.2,"cancelledBy":0,"source":1,"destination":5}]'
#filter by date and sourceddress


@BookADriver
Scenario: Book A Driver
	Given I am a client
	When data items are '{"UserId":1,"DriverId":4,"RideRequestTime":"2021-11-01T05:05:00","Source":3,"Destination":9}'
	And I Make Post Request 'users/1/rides'
	Then response code must be '200'
